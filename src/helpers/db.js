const Sequelize = require('sequelize')

const Op = Sequelize.Op
const sequelizeInstance = new Sequelize(`postgres://${process.env.POSTGRES_USER}:${process.env.POSTGRES_PASSWORD}@${process.env.POSTGRES_SERVER}/${process.env.POSTGRES_DB}`, {
    operatorsAliases: Op,
    define: {
        timestamps: false
    }
})
sequelizeInstance.sync()
module.exports = {sequelizeInstance, Sequelize}