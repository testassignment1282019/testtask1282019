const crypto = require('crypto')
const IV_LENGTH = 16

function encrypt(text, key) {
    const iv = crypto.randomBytes(IV_LENGTH)
    key = key.padEnd(32, '*')
    const cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(key), iv)
    const encrypted = Buffer.concat([cipher.update(text), cipher.final()])
    return { iv: iv.toString('hex'), value: encrypted.toString('hex') }
}

function decrypt(text, key, iv) {
    const ivBuffer =  Buffer.from(iv, 'hex')
    key = key.padEnd(32, '*')
    const encryptedText =  Buffer.from(`${text}:${ivBuffer}`, 'hex')
    const decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(key), ivBuffer)
    const decrypted = Buffer.concat([decipher.update(encryptedText), decipher.final()])
    return decrypted.toString()
}

module.exports = { decrypt, encrypt }
