module.exports = ({ Sequelize, sequelizeInstance }) => {
    return sequelizeInstance.define('entries', {
        id: {
            type: Sequelize.STRING,
            primaryKey: true,
        },
        value: {
            type: Sequelize.TEXT,
            allowNull: false,
        },
        iv: {
            type: Sequelize.STRING,
            allowNull: false,
        }
    })
}