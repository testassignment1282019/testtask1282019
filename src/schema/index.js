const logger = require('winston');
const GraphQLJSON = require('graphql-type-json');
const { encrypt, decrypt } = require('../helpers/encryption');
const { ApolloError, gql } = require('apollo-server');
const sequelize = require('../helpers/db')
const entriesModel = require('../models/entries');
const entries = entriesModel(sequelize)
const Op = sequelize.Sequelize.Op
const typeDefs = gql`
scalar JSONType
   type Entry {
      id: String!
      encryption_key: String!
      value: JSONType!,
    }
    type Query {
      entries(id: String!, encryption_key: String!): [Entry!]!,
    }
    type Mutation {
      addEntry(id: String!, encryption_key: String!,value:JSONType!): String!
    }
`;
const resolvers = {
    JSONType: GraphQLJSON,
    Query: {
        entries: async (_, { id, encryption_key }) => {
            const entriesFromDb = await entries.findAll({
                where: {
                    id: id.includes('*') ? { [Op.like]: id.replace(/\*/g, '%') } : id,
                },
                raw: true
            })
            try {
                entriesFromDb.map((entryFromDb) => {
                    const decryptedValue = decrypt(entryFromDb.value, encryption_key, entryFromDb.iv)
                    return Object.assign(entryFromDb, { value: JSON.parse(decryptedValue) })
                })
            } catch (error) {
                logger.error(`Failed to decrypt for ${id}`)
                return new ApolloError('There was an error processing your request')
            }
            return entriesFromDb
        },
    },
    Mutation: {
        addEntry: async (_, { id, encryption_key, value }) => {
            try {
                if (typeof value !== 'object') {
                    return new ApolloError('JSON type is required, there was an error processing your request')
                }
                const encryptedData = encrypt(JSON.stringify(value), encryption_key)
                await entries.upsert(Object.assign({ id }, encryptedData))
                return "Success"
            } catch (error) {
                return new ApolloError('There was an error processing your request')
            }
        }
    },
};

module.exports = { typeDefs, resolvers }