const logger = require('winston')
const { makeExecutableSchema } = require('graphql-tools')
logger.add(new logger.transports.Console())
const schema = require('./schema/')
const { ApolloServer } = require('apollo-server')
const server = new ApolloServer({schema: makeExecutableSchema(schema)})
server.listen().then(({ url }) => {
    logger.info(`🚀  Server ready at ${url}`)
})
