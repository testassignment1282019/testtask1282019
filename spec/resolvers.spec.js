const encryption = require('../src/helpers/encryption')

describe('Testing resolver', () => {
    it('should encrypt/decrypt', () => {
        const value = 'test'
        const key = 'testencryptionkey'
        const encrypted = encryption.encrypt(value, key)
        const decrypted = encryption.decrypt(encrypted.value, key, encrypted.iv)
        expect(decrypted).toEqual(value)
    })
})